package com.orosasp.mensajes_app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author J Oscar Rosas
 */
public class Conexion {
    
    public Connection getConnection() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mensajes_app", "root", "Roca060413");
            if (connection != null) {
                System.out.println("\nConexion exitosa\n");
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return connection;
    }
    
}
